using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace WebAPI.Models.DataModel
{
    public class PostItems
    {
        [Key]
        public int ID { get; set;}

        
        public string PictureURL { get; set;}

        
        public string PostTime { get; set;}

        public string StatusText { get; set; }

        
        public int UserId { get; set; }
        
        public User User { get; set; }
        

        public ICollection<Like> Likes { get; set; }

        public ICollection<Comment> Comments { get; set; }

    }
}