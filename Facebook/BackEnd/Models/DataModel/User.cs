using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace WebAPI.Models.DataModel
{
    public class User
    {
        [Key]
        public int UserId { get; set;}

        [Required]
        public string Firstname { get; set;}

        [Required]
        public string Surname { get; set;}

        [Required]
        public string Email { get; set;}

        [Required]
        public string Password { get; set;}

        [Required]
        public DateTime Birthday { get; set;}

        [Required]
        public string Gender { get; set;}

        public string ProfilePictureURL { get; set;}


        public ICollection<PostItems> AllPostItems { get; set;}
        
        
    }
}
