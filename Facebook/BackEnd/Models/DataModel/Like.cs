using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace WebAPI.Models.DataModel
{
    public class Like
    {
        [Key]
        public int LikeId { get; set; }

        public int UserId { get; set;}

        public int PostId { get; set; }
        
        public PostItems PostItems { get; set; }      
        
    }
}