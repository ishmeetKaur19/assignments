using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace WebAPI.Models.DataModel
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        public string Text { get; set; }
        
        public int UserId { get; set;}
        
        public int PostId { get; set; }
        public PostItems PostItems { get; set; }
    }
}