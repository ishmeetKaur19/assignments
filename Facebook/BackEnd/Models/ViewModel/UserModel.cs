using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace WebAPI.Models.ViewModel
{
    public class UserModel
    {
        [Key]
        public int UserId { get; set;}

        [Required]
        public string Firstname { get; set;}

        [Required]
        public string Surname { get; set;}

        [Required]
        public string Email { get; set;}

        [Required]
        public string Password { get; set;}

        [Required]
        public DateTime Birthday { get; set;}

        [Required]
        public string Gender { get; set;}
        
        public string ProfilePictureURL { get; set;}
    }
}
