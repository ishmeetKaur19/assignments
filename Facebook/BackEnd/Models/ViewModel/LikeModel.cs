using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace WebAPI.Models.ViewModel
{
    public class LikeModel
    {
        [Key]
        public int LikeId { get; set; }
        public int PictureId { get; set; }
    
        public int UserId { get; set;}
    }
}