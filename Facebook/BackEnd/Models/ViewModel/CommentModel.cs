using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace WebAPI.Models.ViewModel
{
    public class CommentModel
    {
        [Key]
        public int CommentId { get; set; }

        public string Text { get; set; }

        public int UserId { get; set;}
        
        public int PictureId { get; set; }
    }
}