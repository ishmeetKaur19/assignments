using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace WebAPI.Models.ViewModel
{
    public class PostItemsModel
    {
        [Key]
        public int ID { get; set;}

        
        public string PictureURL { get; set;}

        
        public string PostTime { get; set;}

        public string StatusText { get; set; }

        public int CommentCount { get; set; }
        public int LikeCount { get; set; }
        
        public int UserId { get; set; }
        
        

    }
}