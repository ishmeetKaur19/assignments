﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class NameUpdation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_PostItems_PictureId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_PostItems_PictureId",
                table: "Likes");

            migrationBuilder.DropTable(
                name: "Dislikes");

            migrationBuilder.RenameColumn(
                name: "PictureId",
                table: "Likes",
                newName: "PostId");

            migrationBuilder.RenameIndex(
                name: "IX_Likes_PictureId",
                table: "Likes",
                newName: "IX_Likes_PostId");

            migrationBuilder.RenameColumn(
                name: "PictureId",
                table: "Comments",
                newName: "PostId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_PictureId",
                table: "Comments",
                newName: "IX_Comments_PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_PostItems_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "PostItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_PostItems_PostId",
                table: "Likes",
                column: "PostId",
                principalTable: "PostItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_PostItems_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_PostItems_PostId",
                table: "Likes");

            migrationBuilder.RenameColumn(
                name: "PostId",
                table: "Likes",
                newName: "PictureId");

            migrationBuilder.RenameIndex(
                name: "IX_Likes_PostId",
                table: "Likes",
                newName: "IX_Likes_PictureId");

            migrationBuilder.RenameColumn(
                name: "PostId",
                table: "Comments",
                newName: "PictureId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                newName: "IX_Comments_PictureId");

            migrationBuilder.CreateTable(
                name: "Dislikes",
                columns: table => new
                {
                    DislikeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PictureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dislikes", x => x.DislikeId);
                    table.ForeignKey(
                        name: "FK_Dislikes_PostItems_PictureId",
                        column: x => x.PictureId,
                        principalTable: "PostItems",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dislikes_PictureId",
                table: "Dislikes",
                column: "PictureId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_PostItems_PictureId",
                table: "Comments",
                column: "PictureId",
                principalTable: "PostItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_PostItems_PictureId",
                table: "Likes",
                column: "PictureId",
                principalTable: "PostItems",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
