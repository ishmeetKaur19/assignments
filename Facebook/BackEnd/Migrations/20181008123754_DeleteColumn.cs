﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class DeleteColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PicturePostTime",
                table: "PostItems");

            migrationBuilder.RenameColumn(
                name: "StatusPostTime",
                table: "PostItems",
                newName: "PostTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PostTime",
                table: "PostItems",
                newName: "StatusPostTime");

            migrationBuilder.AddColumn<DateTime>(
                name: "PicturePostTime",
                table: "PostItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
