﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class ColumnNamesModify : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Pictures_PictureId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Dislikes_Pictures_PictureId",
                table: "Dislikes");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_Pictures_PictureId",
                table: "Likes");

            migrationBuilder.DropTable(
                name: "Pictures");

            migrationBuilder.CreateTable(
                name: "PostItems",
                columns: table => new
                {
                    PictureID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PictureURL = table.Column<string>(nullable: true),
                    PicturePostTime = table.Column<DateTime>(nullable: false),
                    StatusText = table.Column<string>(nullable: true),
                    StatusPostTime = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostItems", x => x.PictureID);
                    table.ForeignKey(
                        name: "FK_PostItems_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PostItems_UserId",
                table: "PostItems",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_PostItems_PictureId",
                table: "Comments",
                column: "PictureId",
                principalTable: "PostItems",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Dislikes_PostItems_PictureId",
                table: "Dislikes",
                column: "PictureId",
                principalTable: "PostItems",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_PostItems_PictureId",
                table: "Likes",
                column: "PictureId",
                principalTable: "PostItems",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_PostItems_PictureId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Dislikes_PostItems_PictureId",
                table: "Dislikes");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_PostItems_PictureId",
                table: "Likes");

            migrationBuilder.DropTable(
                name: "PostItems");

            migrationBuilder.CreateTable(
                name: "Pictures",
                columns: table => new
                {
                    PictureID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PicturePostTime = table.Column<DateTime>(nullable: false),
                    PictureText = table.Column<string>(nullable: true),
                    StatusPostTime = table.Column<DateTime>(nullable: false),
                    StatusText = table.Column<string>(nullable: true),
                    URL = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pictures", x => x.PictureID);
                    table.ForeignKey(
                        name: "FK_Pictures_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pictures_UserId",
                table: "Pictures",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Pictures_PictureId",
                table: "Comments",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Dislikes_Pictures_PictureId",
                table: "Dislikes",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_Pictures_PictureId",
                table: "Likes",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "PictureID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
