using Microsoft.EntityFrameworkCore;
using WebAPI.Models.DataModel; 
namespace WebAPI.DataContext
{
    public class AppDataContext : DbContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
       {
            modelBuilder.Entity<PostItems>()
            .HasOne<User>(s => s.User)
            .WithMany(g => g.AllPostItems)
            .HasForeignKey(s => s.UserId);

            modelBuilder.Entity<Like>()
            .HasOne<PostItems>(s => s.PostItems)
            .WithMany(g => g.Likes)
            .HasForeignKey(s => s.PostId)
            .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<Comment>()
            .HasOne<PostItems>(s => s.PostItems)
            .WithMany(g => g.Comments)
            .HasForeignKey(s => s.PostId)
            .OnDelete(DeleteBehavior.Cascade);

        
       }

       
         public DbSet<User> Users { get; set; }
         public DbSet<Like> Likes { get; set; }
         public DbSet<Comment> Comments { get; set; }
         public DbSet<PostItems> PostItems { get; set; }
        


    }
}