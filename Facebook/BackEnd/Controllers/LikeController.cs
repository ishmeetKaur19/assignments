using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DataContext;
using WebAPI.Models.DataModel;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LikeController : ControllerBase
    {
        private AppDataContext context;
        public LikeController(AppDataContext context)
        {
            this.context = context;
        }
        // GET api/values
        [HttpGet]
        public IList<LikeModel> GetAll()
        {
            IList<Like> likes = context.Likes.ToList();
            IList<LikeModel> likeModel = likes.Select(x => new LikeModel
            {
                LikeId = x.LikeId,
                UserId = x.UserId,
                PictureId = x.PostId
            }).ToList();
            return likeModel;
        }

        
        [HttpGet("{id}")]
        public ActionResult<LikeModel> GetById(int id)
        {
            Like like = context.Likes.Find(id);
            if (like == null)
            {
                return NotFound();
            }
            LikeModel likeModel = new LikeModel()
            {
                LikeId = like.LikeId,
                UserId = like.UserId,
                PictureId = like.PostId
            };
            return likeModel;
        }

        [HttpGet("likes/{postId}")]
        public IList<LikeModel> GetByPostId(int postId)
        {
            IList<Like> likes = context.Likes.ToList().FindAll(x => x.PostId == postId);
            IList<LikeModel> likeModel = likes.Select(x => new LikeModel
            {
               LikeId = x.LikeId,
               UserId = x.UserId,
               PictureId = x.PostId
            }).ToList();
            
            return likeModel;
        }
        
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Like> Post(LikeModel likeModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Like like = new Like
            {
                LikeId  = likeModel.LikeId,
                PostId = likeModel.PictureId,
                UserId = likeModel.UserId
            };
            context.Likes.Add(like);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = like.LikeId }, like);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Like> Edit(LikeModel likeModel)
        {
        if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Like like = context.Likes.Find(likeModel.UserId);
            like.LikeId  = likeModel.LikeId;
            like.PostId = likeModel.PictureId;
            like.UserId = likeModel.UserId;

            context.Likes.Update(like);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = like.LikeId }, like);
        }

        [HttpDelete("{id}")]
        public ActionResult<Like> Delete(int id)
        {
            Like like = context.Likes.Find(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            context.Likes.Remove(like);
            context.SaveChanges();
            return Ok();
        }
    }
}
