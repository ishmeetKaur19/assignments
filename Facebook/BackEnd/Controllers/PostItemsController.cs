using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DataContext;
using WebAPI.Models.DataModel;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostItemsController : ControllerBase
    {
        private AppDataContext context;
        public PostItemsController(AppDataContext context)
        {
           this.context=context;
        }
       
        [HttpGet]
        public  IList<PostItemsModel> GetAll()
        {
              IList<PostItems> postItemsDb = context.PostItems.ToList();
              IList<PostItemsModel> postItems = postItemsDb.Select(x => new PostItemsModel
            {
                ID = x.ID,
                PictureURL = x.PictureURL,
                PostTime = x.PostTime,
                StatusText = x.StatusText,
                UserId = x.UserId,
                LikeCount = context.Likes.Count(y => y.PostId == x.ID),
                CommentCount = context.Comments.Count(y => y.PostId == x.ID)
            }).ToList();
              return postItems;
        }

        
        [HttpGet("{id}")]
         public ActionResult<PostItemsModel> GetById(int id)
        {
            PostItems postItems = context.PostItems.Find(id);
            if ( postItems == null)
            {
                return NotFound();
            }
            PostItemsModel postItemsModel = new PostItemsModel()
            {
                ID = postItems.ID,
                PictureURL = postItems.PictureURL,
                PostTime = postItems.PostTime,
                StatusText = postItems.StatusText,
                UserId = postItems.UserId,
                CommentCount = context.Comments.Count(x => x.PostId == id),
                LikeCount = context.Likes.Count(x => x.PostId == id)
            };
            return postItemsModel;
        }

        
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<User> Post(PostItemsModel postItemsModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PostItems postItems = new PostItems();
            postItems.PictureURL = postItemsModel.PictureURL;
            postItems.PostTime = postItemsModel.PostTime;
            postItems.StatusText = postItemsModel.StatusText;
            postItems.UserId = postItemsModel.UserId;
            context.PostItems.Add(postItems);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = postItems.UserId }, postItems);
        }
        
        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<PostItems> Edit(PostItemsModel postItemsModel)
        {
        if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PostItems postItems = context.PostItems.Find(postItemsModel.UserId);
            postItems.PictureURL = postItemsModel.PictureURL;
            postItems.PostTime = postItemsModel.PostTime;
            postItems.StatusText = postItemsModel.StatusText;
            postItems.UserId = postItemsModel.UserId;

            context.PostItems.Update(postItems);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = postItems.ID }, postItems);
        }

        [HttpDelete("{id}")]
        public ActionResult<PostItems> Delete(int id)
        {
            PostItems postItems  = context.PostItems.Find(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            context.PostItems.Remove(postItems);
            context.SaveChanges();
            return Ok();
        }
    }
}
