﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DataContext;
using WebAPI.Models.DataModel;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private AppDataContext context;
        public UserController(AppDataContext context)
        {
            this.context = context;
        }

        // GET api/values
        [HttpGet]
        public IList<UserModel> GetAll()
        {
            IList<User> usersDb = context.Users.ToList();

           // var exist = context.Users.Any(u => u.Email == "Ishmeeth@gmail.com");

            IList<UserModel> users = usersDb.Select(x => new UserModel
             {
                 UserId = x.UserId,
                 Firstname = x.Firstname,
                 Surname = x.Surname,
                 Email = x.Email,
                 Password = x.Password,
                 Birthday = x.Birthday,
                 Gender = x.Gender,
                 ProfilePictureURL = x.ProfilePictureURL
             }).ToList();

            return users;
        }

        [HttpGet("{email}/{password}")]
        public ActionResult<Boolean> IsUserExist([FromRoute]string email, string password) {
            var existedUser = context.Users.Any(u => u.Email == email && u.Password == password);
            return existedUser;
        }

        [HttpGet("{email}")]
        public ActionResult<Boolean> IsEmailExist([FromRoute]string email) {
            var existedEmail = context.Users.Any(u => u.Email == email );
            return existedEmail;
        }

        [HttpGet("userById/id/{id}")]
        public ActionResult<UserModel> GetById(int id)
        { 
            User user = context.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }
            UserModel userModel = new UserModel()
            {
                UserId = user.UserId,
                Firstname = user.Firstname,
                Surname = user.Surname,
                Email = user.Email,
                Password = user.Password,
                Birthday = user.Birthday,
                Gender = user.Gender,
                ProfilePictureURL = user.ProfilePictureURL
            };
            return userModel;
        }


        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<User> Post(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = new User();
            user.Firstname = userModel.Firstname;
            user.Surname = userModel.Surname;
            user.Email = userModel.Email;
            user.Password = userModel.Password;
            user.Birthday = userModel.Birthday;
            user.Gender = userModel.Gender;
            user.ProfilePictureURL = userModel.ProfilePictureURL;

            context.Users.Add(user);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = user.UserId }, user);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<User> Edit(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = context.Users.Find(userModel.UserId);
            user.Firstname = userModel.Firstname;
            user.Surname = userModel.Surname;
            user.Email = userModel.Email;
            user.Password = userModel.Password;
            user.Birthday = userModel.Birthday;
            user.Gender = userModel.Gender;
            user.ProfilePictureURL = userModel.ProfilePictureURL;

            context.Users.Update(user);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = user.UserId }, user);
        }


        [HttpDelete]
        public ActionResult<User> Delete(int id)
        {
            //null check
            User user = context.Users.Find(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            context.Users.Remove(user);
            context.SaveChanges();
            return Ok();
        }
    }
}
