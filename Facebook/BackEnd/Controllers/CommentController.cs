using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DataContext;
using WebAPI.Models.DataModel;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private AppDataContext context;
        public CommentController(AppDataContext context)
        {
            this.context = context;
        }
        // GET api/values
        [HttpGet]
        public IList<CommentModel> GetAll()
        {
            IList<Comment> comments = context.Comments.ToList();
            IList<CommentModel> commentModel = comments.Select(x => new CommentModel
            {
                CommentId = x.CommentId,
                Text = x.Text,
                UserId = x.UserId,
                PictureId = x.PostId
            }).ToList();
            return commentModel;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Comment> GetById(int id)
        {
            Comment comment = context.Comments.Find(id);
            if (comment == null)
            {
                return NotFound();
            }
            CommentModel commentModel = new CommentModel()
            {
                CommentId = comment.CommentId,
                Text = comment.Text,
                UserId = comment.UserId,
                PictureId = comment.PostId
            };
            return comment;
        }

        [HttpGet("comments/{postId}")]
        public IList<CommentModel> GetByPostId(int postId)
        {
            IList<Comment> comments = context.Comments.ToList().FindAll(x => x.PostId == postId);
            IList<CommentModel> commentModel = comments.Select(x => new CommentModel
            {
               CommentId = x.CommentId,
               Text = x.Text,
               UserId = x.UserId,
               PictureId = x.PostId 
            }).ToList();
            
            return commentModel;
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Comment> Post(CommentModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Comment comment = new Comment
            {
                CommentId  = commentModel.CommentId,
                Text = commentModel.Text,
                PostId = commentModel.PictureId,
                UserId = commentModel.UserId
            };
            context.Comments.Add(comment);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = comment.CommentId }, comment);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Comment> Edit(CommentModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            Comment comment = context.Comments.Find(commentModel.CommentId);
            comment.CommentId  = commentModel.CommentId;
            comment.Text = commentModel.Text;
            comment.PostId = commentModel.PictureId;
            comment.UserId = commentModel.UserId;

            context.Comments.Update(comment);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = comment.CommentId }, comment);
        }

        // DELETE api/values/5
        [HttpDelete]
        public ActionResult<Comment> Delete(int id)
        {
            Comment comment = context.Comments.Find(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            context.Comments.Remove(comment);
            context.SaveChanges();
            return Ok();
        }
    }
}
