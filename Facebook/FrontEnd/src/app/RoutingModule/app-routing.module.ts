import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../Components/login/login.component';
import { RegistrationComponent } from '../Components/registration/registration.component';
import { HomePageComponent } from '../Components/home-page/home-page.component';
import { CommentComponent } from '../Components/comment/comment.component';
import { AuthGuard } from '../auth.guard';
import { DisplayComponent } from '../Components/display/display.component';

const routes: Routes = [
  { path: '', redirectTo: '/registration', pathMatch: 'full'},
  {path: 'registration', component: RegistrationComponent},
  {path: 'home-page', component: HomePageComponent,
      children: [
        {path: 'display' , component: DisplayComponent,
               children: [
                 {path: 'comments' , component: CommentComponent}
               ]}
      ]},
  {path: '**', component: RegistrationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
