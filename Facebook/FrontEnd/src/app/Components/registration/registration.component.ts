import { Component, OnInit } from '@angular/core';
import { User } from '../../Classes/User';
import { RegistrationService } from '../../Services/registration.service';
import { AuthService } from '../../Services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

  registrationObject = new User();
  Users = [];
  isAuthorizedSuccessful = false;
  url;
  isEmailExists = false;
  profilePictureURL: string;
  isPictureIsSelected = false;

  constructor(private registerService: RegistrationService,
  private router: Router, private Auth: AuthService, private toastr: ToastrService) { }

  ngOnInit() {
  }


  previewUpload(event: any) {
    this.isPictureIsSelected = true;
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
      };
      reader.readAsDataURL(event.target.files[0]);
      this.profilePictureURL = event.target.files[0].name;
    }
  }

  signUp(email) {
    this.registerService.isEmailExist(email).subscribe(
      response => {
        this.isEmailExists = response;
        if (this.isEmailExists === true) {
            this.toastr.warning('Email already exists!', 'Try again');
          } else {
            this.registrationObject.profilePictureURL = '../../assets/' + this.profilePictureURL;
            this.registerService.createUser(this.registrationObject).subscribe(
              res => {
                console.log(res);
              }
            );
            this.toastr.success('You have registered successfully. Please login to continue', 'Congrats');
          }
      }
    );
    this.isEmailExists = false;
  }
}
