import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationComponent } from './registration.component';
import { HomePageComponent } from '../home-page/home-page.component';
import { LoginComponent } from '../login/login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../../RoutingModule/app-routing.module';
import { FormsModule } from '@angular/forms';
import { DisplayComponent } from '../display/display.component';
import { CommentComponent } from '../comment/comment.component';
import { RegistrationService } from '../../Services/registration.service';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DebugElement } from '@angular/core';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        HttpClientTestingModule,
        RouterModule,
        AppRoutingModule,
        FormsModule
      ],
      declarations: [RegistrationComponent,
        HomePageComponent,
        LoginComponent,
        DisplayComponent,
        CommentComponent
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' },
        RegistrationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have <h1> with "Create an account"', () => {
    const p = fixture.nativeElement.querySelector('h1');
    expect(p.textContent).toEqual('Create an account');
  });
});
