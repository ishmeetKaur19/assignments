import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../../RoutingModule/app-routing.module';
import { FormsModule } from '@angular/forms';
import { RegistrationComponent } from '../registration/registration.component';
import { HomePageComponent } from '../home-page/home-page.component';
import { DisplayComponent } from '../display/display.component';
import { CommentComponent } from '../comment/comment.component';
import { APP_BASE_HREF } from '@angular/common';
import { RegistrationService } from '../../Services/registration.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { User } from '../../Classes/User';
import { of } from 'rxjs';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userServiceStub: any;
  let getAllUsersSpy: any;

  beforeEach(async(() => {
    const expectedResult: User[] = [
      {
      'userId': 26,
      'firstname': 'Jane',
      'surname': 'Austin',
      'email': 'jane@gmail.com',
      'password': 'jane',
      'birthday': '1996-01-19T00:00:00',
      'gender': 'female',
      'profilePictureURL': '../../assets/images.jpeg'
     },
     {
       'userId': 27,
       'firstname': 'James ',
       'surname': 'wan',
       'email': 'james@gmail.com',
       'password': 'james',
       'birthday': '1990-03-20T00:00:00',
       'gender': 'male',
       'profilePictureURL': '../../assets/jamesWan.jpeg'
      },
      {
       'userId': 28,
       'firstname': 'Himanshi',
       'surname': 'sharma',
       'email': 'himanshi@gmail.com',
       'password': 'himanshi',
       'birthday': '1990-12-20T00:00:00',
       'gender': 'female',
       'profilePictureURL': '../../assets/51b91bba5a3fd9b6c8b9c53bc0ab6c65.jpg'
      },
      {
        'userId': 29,
        'firstname': 'Hardik',
        'surname': 'Mittal',
        'email': 'hardik@gmail.com',
        'password': 'hardik',
        'birthday': '1998-03-18T00:00:00',
        'gender': 'male',
        'profilePictureURL': '../../assets/hardik.jpeg'
      }
    ];

    userServiceStub = jasmine.createSpyObj('RegistrationService', ['getAllUsers']);
    getAllUsersSpy = userServiceStub.getAllUsers.and.returnValue(of(expectedResult));
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        HttpClientTestingModule,
        RouterModule,
        AppRoutingModule,
        FormsModule
      ],
      declarations: [RegistrationComponent,
        HomePageComponent,
        LoginComponent,
        DisplayComponent,
        CommentComponent
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' },
        RegistrationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have as title dash', async(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.debugElement.componentInstance;
    expect(component.title).toEqual('Login-page');
  }));

  it('On entering email and password of the user will emit loggedIn event', () => {
    // tslint:disable-next-line:prefer-const
    let user: User;
});

it('should authorised if credentialsNotMatched evaluates to false', () => {
  const hostElement = fixture.nativeElement;
  component.registrationObj.email = 'himanshi@gmail.com';
  component.registrationObj.password = 'himanshi';
  getAllUsersSpy();
  fixture.detectChanges();
  console.log(component.Users);
  console.log(component.credentialsNotMatched);
  component.login(component.registrationObj.email, component.registrationObj.password);
  console.log(component.credentialsNotMatched);

  expect(component.credentialsNotMatched).toEqual(false, component.credentialsNotMatched);
});

});
