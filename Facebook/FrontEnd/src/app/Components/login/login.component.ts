import { Component, OnInit } from '@angular/core';
import {User} from '../../Classes/User';
import { RegistrationService } from '../../Services/registration.service';
import { Router } from '@angular/router';
import { AuthService } from '../../Services/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registrationObj = new User();
  Users = [];
  isAuthorizedSuccessful = false;
  userId: number;
  title = 'Login-page';
  credentialsNotMatched = false;

  constructor(private toastr: ToastrService, private register: RegistrationService,
    private router: Router, private Auth: AuthService) {
     }

  ngOnInit() {
    this.register.getAllUsers().subscribe(
      res => {
        this.Users = res;
      });
  }

  login(email, password) {
    this.register.getAllUsers().subscribe(
      res => {
        this.Users = res;
        for (const user of this.Users) {
          const registrationObject: User = user;
          if ((email === registrationObject.email) && (password === registrationObject.password)) {
             this.isAuthorizedSuccessful = true;
             this.userId = registrationObject.userId;
          }
       }
       if (this.isAuthorizedSuccessful === true) {
          this.credentialsNotMatched = false;
          this.Auth.setLoggedIn(true);
          this.toastr.success('You have logged in successfully', 'Congrats');
          this.router.navigate(['/home-page'], { queryParams: { Id: this.userId }});
       } else {
          this.credentialsNotMatched = true;
          this.toastr.warning('Please enter valid email or password' , 'Enter valid credentials');
       }
      });
    }

}
