import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {APP_BASE_HREF} from '@angular/common';

import { CommentComponent } from './comment.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../RoutingModule/app-routing.module';
import { RegistrationComponent } from '../registration/registration.component';
import { HomePageComponent } from '../home-page/home-page.component';
import { DisplayComponent } from '../display/display.component';
import { LoginComponent } from '../login/login.component';
import { RegistrationService } from '../../Services/registration.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
      CommentComponent,
      RegistrationComponent,
      HomePageComponent,
      DisplayComponent,
      LoginComponent
    ],
      imports: [
        HttpClientTestingModule,
        RouterModule,
        AppRoutingModule,
        FormsModule
      ],
      providers: [{provide: APP_BASE_HREF, useValue : '/' },
      RegistrationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
