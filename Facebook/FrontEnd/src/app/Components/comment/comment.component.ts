import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../Classes/Comment';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService } from '../../Services/comment.service';
import { PostItem } from '../../Classes/PostItem';
import { RegistrationService } from '../../Services/registration.service';
import { User } from '../../Classes/User';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() post;
  @Output() countCommentChanged: EventEmitter<any> = new EventEmitter();
  commentObj = new Comment();
  comments = [];
  commentsDisplay = [];
  userId: number;
  show = true;
  Users = [];
  userName: string;
  pictureURL: string;
  commentsByPostId = [];

  constructor(private route: ActivatedRoute, private commentService: CommentService,
    private router: Router, private registerService: RegistrationService) { }

  ngOnInit() {
    this.getAllCommentsAndUsers();
  }

  getAllCommentsAndUsers() {
    this.commentService.getAllComments().subscribe(
      res => {
        this.comments = res;
        this.registerService.getAllUsers().subscribe(
          resp => {
            this.Users = resp;
            this.fetchId();
            this.displayComments();
          });
      });
  }

  fetchId() {
    this.route.queryParams
      .filter(params => params.Id)
      .subscribe(params => {
        this.userId = params.Id;
      });
  }

  displayComments() {
    this.commentsDisplay = [];
    const postItemObj: PostItem = this.post;
    this.commentService.getCommentsByPostId(postItemObj.id).subscribe(
      resp => {
        this.commentsByPostId = resp;
        for (const comment of this.commentsByPostId) {
          const commentObj: Comment = comment;
          this.fetchUserName(commentObj.userId);
          const obj = {
            profilePictureURL: this.pictureURL,
            userName: this.userName,
            text: commentObj.text
          };
          this.commentsDisplay.push(obj);
        }
      }
    );
  }

  fetchUserName(userId) {
    for (const user of this.Users) {
      const userObj: User = user;
      // tslint:disable-next-line:triple-equals
      if (userId == userObj.userId) {
        this.userName = userObj.firstname + ' ' + userObj.surname;
        this.pictureURL = userObj.profilePictureURL;
      }

    }
  }

  postComment() {
    const postItemObj: PostItem = this.post;
    this.commentObj.pictureId = postItemObj.id;
    this.commentObj.userId = this.userId;
    this.commentService.createComment(this.commentObj).subscribe(
      res => {
        this.commentService.getAllComments().subscribe(
          resp => {
            this.comments = resp;
            this.displayComments();
          });
        this.countCommentChanged.emit(this.commentObj);
      }
    );
    this.router.navigate(['home-page'], { queryParams: { Id: this.userId } });
    this.show = false;
  }

  navigateToHomePage() {
    this.router.navigate(['home-page'], { queryParams: { Id: this.userId } });
  }

}
