import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageComponent } from './home-page.component';
import { AppComponent } from '../../app.component';
import { RegistrationComponent } from '../registration/registration.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../../RoutingModule/app-routing.module';
import { FormsModule } from '@angular/forms';
import { DisplayComponent } from '../display/display.component';
import { CommentComponent } from '../comment/comment.component';
import { LoginComponent } from '../login/login.component';
import { RegistrationService } from '../../Services/registration.service';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RegistrationComponent,
        HomePageComponent,
        DisplayComponent,
        CommentComponent,
        LoginComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterModule,
        AppRoutingModule,
        FormsModule
      ],
      providers: [{provide: APP_BASE_HREF, useValue : '/' },
      RegistrationService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
