import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegistrationService } from '../../Services/registration.service';
import 'rxjs/add/operator/filter';
import { User } from '../../Classes/User';
import { Router } from '@angular/router';
import { PostItem } from '../../Classes/PostItem';
import { PostService } from '../../Services/post.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  registrationObj = new User();
  userId: number;
  userName: string;
  Users = [];
  profilePictureURL: string;
  isUploadStatus = false;
  isUploadPicture = false;
  postItemObj = new PostItem();
  url;
  Posts = [];
  d = new Date();
  pictureURL: string;
  isPictureIsSelected = false;
  isNewPostCreate = false;



  constructor(private route: ActivatedRoute, private postService: PostService,
    private router: Router, private register: RegistrationService) { }

  ngOnInit() {
    this.register.getAllUsers().subscribe(
      res => {
        this.Users = res;
        this.fetchId();
      });
    this.postService.getAllPosts().subscribe(
      res => {
        this.Posts = res;
      });

  }

  fetchId() {
    this.route.queryParams
      .filter(params => params.Id)
      .subscribe(params => {
        this.userId = params.Id;
      });
    this.fetchLoggedInUserInfo();
  }

  navigateToHomePage() {
    this.isUploadPicture = false;
  }

  uploadStatus() {
    const statusPostTime: string = this.d.getDate() + '-' + this.d.getMonth() + '-' + this.d.getFullYear() + ' ' +
      this.d.getHours() + ':' + this.d.getMinutes();
    this.postItemObj.postTime = statusPostTime;
    this.postItemObj.userId = this.userId;
    this.postService.createPost(this.postItemObj).subscribe(
      res => {
        this.isNewPostCreate = true;
      }
    );

    this.resetUploadStatusTab();
  }

  uploadPicture() {
    const statusPostTime: string = this.d.getDate() + '-' + this.d.getMonth() + '-' + this.d.getFullYear() + ' ' +
    this.d.getHours() + ':' + this.d.getMinutes();
    this.postItemObj.postTime = statusPostTime;
    this.postItemObj.userId = this.userId;
    this.postItemObj.pictureURL = '../../assets/' + this.pictureURL;
    this.postService.createPost(this.postItemObj).subscribe(
      res => {
        this.isNewPostCreate = true;
      }
    );
    this.resetUploadPictureTab();
  }

  resetUploadStatusTab() {
    this.isNewPostCreate = false;
    this.isUploadStatus = false;
    this.postItemObj.statusText = '';
  }

  resetUploadPictureTab() {
    this.isNewPostCreate = false;
    this.isUploadPicture = false;
    this.isPictureIsSelected = false;
    this.postItemObj.statusText = '';
  }

  fetchLoggedInUserInfo() {
    for (const user of this.Users) {
      this.registrationObj = user;
      // tslint:disable-next-line:triple-equals
      if (this.userId == this.registrationObj.userId) {
        this.userName = this.registrationObj.firstname + ' ' + this.registrationObj.surname;
        this.profilePictureURL = this.registrationObj.profilePictureURL;
      }
    }
  }

  navigateToUploadStatusPage() {
    // this.router.navigate(['home-page/upload-status'], { queryParams: { Id: this.userId }});
    this.isUploadStatus = true;
    this.isUploadPicture = false;
  }

  navigateToUploadPicturePage() {
    // this.router.navigate(['home-page/upload-picture'], { queryParams: { Id: this.userId }});
    this.isUploadPicture = true;
    this.isUploadStatus = false;
  }


  closeStatusTab() {
    this.isUploadStatus = false;
  }

  closePictureTab() {
    this.isUploadPicture = false;
  }

  previewUpload(event: any) {
    this.isPictureIsSelected = true;
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event: ProgressEvent) => {
        this.url = (<FileReader>event.target).result;
      };
      reader.readAsDataURL(event.target.files[0]);
      this.pictureURL = event.target.files[0].name;
    }
  }
}
