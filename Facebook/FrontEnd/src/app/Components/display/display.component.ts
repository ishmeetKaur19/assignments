import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../../Services/post.service';
import { PostItem } from '../../Classes/PostItem';
import { ActivatedRoute } from '@angular/router';
import { RegistrationService } from '../../Services/registration.service';
import { User } from '../../Classes/User';
import { Router } from '@angular/router';
import { LikeService } from '../../Services/like.service';
import { Like } from '../../Classes/Like';
import { Comment } from '../../Classes/Comment';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  postItems: PostItem[] = [];
  userId: number;
  Users = [];
  userObj = new User();
  userName: string;
  profilePictureURL: string;
  show = [];
  likeObj = new Like();
  likes = [];
  userIdExists = false;
  postWithUserDetails: PostItem[] = [];
  updatePost: PostItem;
  likedUsers = [];
  showLikedUsersTab = [];
  likesByPostId = [];


  @Input()
  set isNewPostCreate(isNewPostCreate: boolean) {
    console.log(isNewPostCreate);
    this.getPostWithUserDetails();
  }

  constructor(private postService: PostService, private router: Router,
    private route: ActivatedRoute, private registerService: RegistrationService, private likeService: LikeService) { }

  ngOnInit() {
    this.getAllPostsAndUsers();
  }

  getAllPostsAndUsers() {
    this.postService.getAllPosts().subscribe(
      res => {
        this.postItems = res;
        this.registerService.getAllUsers().subscribe(
          resp => {
            this.Users = resp;
            console.log(this.Users);
            this.fetchId();
          });
      });
  }

  deletePost(post) {
    this.postService.deletePost(post.id).subscribe(
      res => {
        this.postService.getAllPosts().subscribe(
          resp => {
            this.postItems = resp;
            this.getPostWithUserDetails();
          });
      }
    );
  }

  getPostWithUserDetails() {
    this.postWithUserDetails = [];
    this.postService.getAllPosts().subscribe(
      resp => {
        this.postItems = resp;
        for (const post of this.postItems) {
          const postItemObj: PostItem = post;
          this.postWithUserDetails.unshift(postItemObj);
        }
        for (const postItem of this.postWithUserDetails) {
          const postItemObj: PostItem = postItem;
          console.log(postItemObj.userId);
          this.registerService.getUserById(postItemObj.userId).subscribe(
            res => {
              postItemObj.users = res;
              console.log(res);              // tslint:disable-next-line:triple-equals
              if (postItemObj.userId == this.userId) {
                postItemObj.settings = true;
              } else {
                postItemObj.settings = false;
              }
            }
          );
        }
        console.log(this.postWithUserDetails);
      });
  }

  countCommentChangedHandler(comment) {
    const newComment: Comment = comment;
    this.postService.getPostById(newComment.pictureId).subscribe(
      res => {
        this.updatePost = res;
        for (const postItem of this.postWithUserDetails) {
          const postItemObj: PostItem = postItem;
          if (postItemObj.id === this.updatePost.id) {
            postItemObj.commentCount = this.updatePost.commentCount;
          }
        }
      }
    );
  }

  fetchId() {
    this.route.queryParams
      .filter(params => params.Id)
      .subscribe(params => {
        this.userId = params.Id;
      });
  }

  toggleLikedUsersTab(post) {
    const postItemObj: PostItem = post;
    this.showLikedUsersTab[postItemObj.id] = !this.showLikedUsersTab[postItemObj.id];
  }

  toggleCommentTab(post) {
    const postItemObj: PostItem = post;
    this.show[postItemObj.id] = !this.show[postItemObj.id];
  }

  likedPost(status) {
    const postItemObj: PostItem = status;
    this.likeService.getAllLikes().subscribe(
      res => {
        this.likes = res;
        for (const like of this.likes) {
          const likeObj: Like = like;
          // tslint:disable-next-line:triple-equals
          if ((this.userId == likeObj.userId) && (postItemObj.id == likeObj.pictureId)) {
            this.userIdExists = true;
            this.likeService.deleteLike(likeObj.likeId).subscribe(
            respo => {
            this.postService.getPostById(likeObj.pictureId).subscribe(
              response => {
                this.updatePost = response;
                for (const postItem of this.postWithUserDetails) {
                  const postItemObjNew: PostItem = postItem;
                  if (postItemObjNew.id === this.updatePost.id) {
                    postItemObj.likeCount = this.updatePost.likeCount;
                  }
                }
              });
            });
            }
        }
        if (this.userIdExists === false) {
          this.likeObj.pictureId = postItemObj.id;
          this.likeObj.userId = this.userId;
          this.createLike(this.likeObj, status);
        }
      });
    this.userIdExists = false;
  }

  createLike(likeObj, post) {
    const postItemObj: PostItem = post;
    this.likeService.createLike(likeObj).subscribe(
      respo => {
        this.postService.getPostById(likeObj.pictureId).subscribe(
          response => {
            this.updatePost = response;
            for (const postItem of this.postWithUserDetails) {
              const postItemObjNew: PostItem = postItem;
              if (postItemObjNew.id === this.updatePost.id) {
                postItemObj.likeCount = this.updatePost.likeCount;
              }
            }
          });
      }
    );
  }

  showAllLikedUsers(post) {
    this.likedUsers = [];
    this.likeService.getLikesByPostId(post.id).subscribe(
      res => {
        this.likesByPostId = res;
        for (const like of this.likesByPostId) {
          const likeObj: Like = like;
          this.fetchUserName(likeObj.userId);
          const obj = {
            userName: this.userName
          };
          this.likedUsers.push(obj);
        }
      }
    );
  }

  fetchUserName(userId) {
    for (const user of this.Users) {
      const userObj: User = user;
      // tslint:disable-next-line:triple-equals
      if (userId == userObj.userId) {
        this.userName = userObj.firstname + ' ' + userObj.surname;
      }

    }
  }
}
