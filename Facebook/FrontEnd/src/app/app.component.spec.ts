import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './RoutingModule/app-routing.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RegistrationComponent } from './Components/registration/registration.component';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { DisplayComponent } from './Components/display/display.component';
import { CommentComponent } from './Components/comment/comment.component';
import { LoginComponent } from './Components/login/login.component';
import { RegistrationService } from './Services/registration.service';
import { APP_BASE_HREF } from '@angular/common';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RegistrationComponent,
        HomePageComponent,
        DisplayComponent,
        CommentComponent,
        LoginComponent
      ],
      imports: [
        RouterModule,
        AppRoutingModule,
        FormsModule
      ],
      providers: [{provide: APP_BASE_HREF, useValue : '/' },
      RegistrationService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  /* it(`should have as title 'PostingApplication'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('PostingApplication');
  })); */
  /* it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to PostingApplication!');
  })); */
});
