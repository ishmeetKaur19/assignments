import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Like } from '../Classes/Like';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LikeService {

  api = 'https://localhost:5001/api';

  constructor( private _http: HttpClient) { }

  getAllLikes(): Observable<Like[]> {
    const apiURL = `${this.api}/like`;
    return this._http.get<Like[]>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
  }

  createLike (post: Like): Observable<Like> {
    const apiURL = `${this.api}/like`;
    return this._http.post<Like>(apiURL, post)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
}

getLikeById(id: number): Observable<Like> {
  const apiURL = `${this.api}/like/${id}`;
  return this._http.get<Like>(apiURL)
  .pipe(catchError ((error) => {
    return this.errorHandler(error);
  }) as any);
}

deleteLike (id: number): Observable<Like> {
  const apiURL = `${this.api}/like/${id}`;
  return this._http.delete(apiURL)
  .pipe(catchError((error) => {
    return this.errorHandler(error); }) as any);
 }

 getLikesByPostId (postId: number): Observable<Like[]> {
  const apiURL = `${this.api}/like/likes/${postId}`;
  return this._http.get<Like[]>(apiURL)
  .pipe(catchError ((error) => {
    return this.errorHandler(error);
  }) as any);
}
errorHandler(error: HttpErrorResponse) {
  return throwError (error.message);
}

}
