import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../Classes/User';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

   api = 'https://localhost:5001/api';

  constructor( private _http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    const apiURL = `${this.api}/user`;
    console.log(apiURL);
    return this._http.get<User[]>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
  }

  isUserExist(email: string, password: string): Observable<boolean> {
    const apiURL = `${this.api}/user/${email}/${password}`;
    return this._http.get<boolean>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    })as any);
  }

  isEmailExist(email: string): Observable<boolean> {
    const apiURL = `${this.api}/user/${email}`;
    return this._http.get<boolean>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    })as any);
  }

 getUserById(id: number): Observable<User> {
    const apiURL = `${this.api}/user/userById/id/${id}`;
    return this._http.get<User>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
 }

 createUser (user: User): Observable<User> {
  const apiURL = `${this.api}/user`;
  return this._http.post<User>(apiURL, user)
  .pipe(catchError((error) => {
    return this.errorHandler(error); }) as any);
}

 deleteUser (id: number): Observable<User> {
  const apiURL = `${this.api}/user/${id}`;
  return this._http.delete(apiURL)
  .pipe(catchError((error) => {
    return this.errorHandler(error); }) as any);
 }

errorHandler(error: HttpErrorResponse) {
  return throwError (error.message);
}
}
