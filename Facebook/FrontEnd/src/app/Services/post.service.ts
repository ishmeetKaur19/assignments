import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PostItem } from '../Classes/PostItem';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

   api = 'https://localhost:5001/api';

  constructor( private _http: HttpClient) { }

  getAllPosts(): Observable<PostItem[]> {
    const apiURL = `${this.api}/postItems`;
    return this._http.get<PostItem[]>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
  }

  createPost (post: PostItem): Observable<PostItem> {
    const apiURL = `${this.api}/postItems`;
    return this._http.post<PostItem>(apiURL, post)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
}

getPostById(id: number): Observable<PostItem> {
  const apiURL = `${this.api}/postItems/${id}`;
  return this._http.get<PostItem>(apiURL)
  .pipe(catchError ((error) => {
    return this.errorHandler(error);
  }) as any);
}

deletePost (id: number): Observable<PostItem> {
  const apiURL = `${this.api}/postItems/${id}`;
  return this._http.delete(apiURL)
  .pipe(catchError((error) => {
    return this.errorHandler(error); }) as any);
 }

errorHandler(error: HttpErrorResponse) {
  return throwError (error.message);
}



}
