import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { defer } from 'rxjs';
import { RegistrationService } from './registration.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from '../RoutingModule/app-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RegistrationComponent } from '../Components/registration/registration.component';
import { HomePageComponent } from '../Components/home-page/home-page.component';
import { DisplayComponent } from '../Components/display/display.component';
import { CommentComponent } from '../Components/comment/comment.component';
import { LoginComponent } from '../Components/login/login.component';
import { APP_BASE_HREF } from '@angular/common';
import { User } from '../Classes/User';


let httpClientSpy: { get: jasmine.Spy };
let userService: RegistrationService;

/** Create async observable that emits-once and completes
 *  after a JS engine turn */
export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

/** Create async observable error that errors
*  after a JS engine turn */
export function asyncError<T>(errorObject: any) {
  return defer(() => Promise.reject(errorObject));
}


describe('RegistrationService', () => {

  beforeEach(() => TestBed.configureTestingModule({

    declarations: [
      RegistrationComponent,
      HomePageComponent,
      DisplayComponent,
      CommentComponent,
      LoginComponent
    ],
    imports: [
      HttpClientTestingModule,
      BrowserAnimationsModule,
      ToastrModule.forRoot(),
      RouterModule,
      AppRoutingModule,
      FormsModule
    ],
    providers: [
      {provide: APP_BASE_HREF, useValue : '/' },
      RegistrationService,
    ]
  }));
  beforeEach( () => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    userService = new RegistrationService(<any>httpClientSpy);
  });

  it('should be created', () => {
    userService = TestBed.get(RegistrationService);
    expect(userService).toBeTruthy();
  });

  it('should return expect users', fakeAsync(() => {
    const expectedResult: User[] = [
      {
      'userId': 26,
      'firstname': 'Jane',
      'surname': 'Austin',
      'email': 'jane@gmail.com',
      'password': 'jane',
      'birthday': '1996-01-19T00:00:00',
      'gender': 'female',
      'profilePictureURL': '../../assets/images.jpeg'
     },
     {
       'userId': 27,
       'firstname': 'James ',
       'surname': 'wan',
       'email': 'james@gmail.com',
       'password': 'james',
       'birthday': '1990-03-20T00:00:00',
       'gender': 'male',
       'profilePictureURL': '../../assets/jamesWan.jpeg'
      },
      {
       'userId': 28,
       'firstname': 'Himanshi',
       'surname': 'sharma',
       'email': 'himanshi@gmail.com',
       'password': 'himanshi',
       'birthday': '1990-12-20T00:00:00',
       'gender': 'female',
       'profilePictureURL': '../../assets/51b91bba5a3fd9b6c8b9c53bc0ab6c65.jpg'
      },
      {
        'userId': 29,
        'firstname': 'Hardik',
        'surname': 'Mittal',
        'email': 'hardik@gmail.com',
        'password': 'hardik',
        'birthday': '1998-03-18T00:00:00',
        'gender': 'male',
        'profilePictureURL': '../../assets/hardik.jpeg'
      }
    ];

    httpClientSpy.get.and.returnValue(asyncData(expectedResult));

    userService.getAllUsers().subscribe(
      users => expect(users).toEqual(expectedResult, 'expectedUsers'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  }));

  it('should return expected user on passing userId(HttpClient called once)', () => {
    let expectedUser: User;
    expectedUser = {
      'userId': 26,
      'firstname': 'Jane',
      'surname': 'Austin',
      'email': 'jane@gmail.com',
      'password': 'jane',
      'birthday': '1996-01-19T00:00:00',
      'gender': 'female',
      'profilePictureURL': '../../assets/images.jpeg'
    };

    httpClientSpy.get.and.returnValue(asyncData(expectedUser));

    userService.getUserById(26).subscribe(
      user => expect(user).toEqual(expectedUser, expectedUser.firstname),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });
});
