import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Comment } from '../Classes/Comment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  api = 'https://localhost:5001/api';

  constructor( private _http: HttpClient) { }

  getAllComments(): Observable<Comment[]> {
    const apiURL = `${this.api}/comment`;
    return this._http.get<Comment[]>(apiURL)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
  }

  createComment (post: Comment): Observable<Comment> {
    const apiURL = `${this.api}/comment`;
    return this._http.post<Comment>(apiURL, post)
    .pipe(catchError ((error) => {
      return this.errorHandler(error);
    }) as any);
}

getCommentsByPostId (postId: number): Observable<Comment[]> {
  const apiURL = `${this.api}/comment/comments/${postId}`;
  return this._http.get<Comment[]>(apiURL)
  .pipe(catchError ((error) => {
    return this.errorHandler(error);
  }) as any);
}

getCommentById(id: number): Observable<Comment> {
  const apiURL = `${this.api}/comment/${id}`;
  return this._http.get<Comment>(apiURL)
  .pipe(catchError ((error) => {
    return this.errorHandler(error);
  }) as any);
}

deleteComment (id: number): Observable<Comment> {
  const apiURL = `${this.api}/comment/${id}`;
  return this._http.delete(apiURL)
  .pipe(catchError((error) => {
    return this.errorHandler(error); }) as any);
 }

errorHandler(error: HttpErrorResponse) {
  return throwError (error.message);
}


}
