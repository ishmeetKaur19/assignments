export class Comment {
    public id: number;
    public text: string;
    public pictureId: number;
    public userId: number;
}
