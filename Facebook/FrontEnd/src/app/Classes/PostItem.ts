import { User } from './User';

export class PostItem {
    public id: number;
    public pictureURL: string;
    public postTime: string;
    public statusText: string;
    public userId: number;
    public likeCount: number;
    public commentCount: number;
    public users?: User;
    public settings?: boolean;
}
