export class User {
        public userId: number;
        public firstname: string;
        public surname: string;
        public email: string;
        public password: string;
        public birthday: string;
        public gender: string;
        public profilePictureURL: string;
 }
